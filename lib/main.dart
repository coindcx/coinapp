import 'package:coindcx/ui/market/market_details_page.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'CoinDCX',
        theme: ThemeData(
          /* canvasColor: Colors.transparent,*/
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: SplashScreenPage() //(/*title: 'Market'*/),
        );
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreenPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration(seconds: 2), () {
      ////print("sss");

      openApp();
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget networkSvg = SvgPicture.asset(
      'assets/images/app_icon.svg',
      semanticsLabel: 'A shark?!',
      height: 100,
      width: 100,
      placeholderBuilder: (BuildContext context) => Container(
          padding: const EdgeInsets.all(10.0),
          child: const CircularProgressIndicator()),
    );
    return Scaffold(
      body: Container(
        child: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                networkSvg,
                SizedBox(
                  height: 24,
                ),
                Container(
                  child: Text(
                    "Welcome to CoinDCX Markets",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black87, fontSize: 24),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> openApp() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    print("connectivityResult");
    print(connectivityResult);
    if (connectivityResult == ConnectivityResult.mobile) {
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MarketDetailPage()),
      );
    } else if (connectivityResult == ConnectivityResult.wifi) {
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MarketDetailPage()),
      );
    } else if (ConnectivityResult.none == connectivityResult) {
      /* Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MarketDetailPage()),
      );*/
      showInterNetAlert();
    }
  }

  void showInterNetAlert() {
    /*showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => */ /*Dialog(
          child:*/ /* */ /*,
            contentPadding: EdgeInsets.all(0),
            children: <Widget>[*/ /*
              FancyDialog(
                title: "No internet connection",
                descreption:
                    "There is no internet connection. Please turn on your internet connection and press continue.",
                */ /*animationType: FancyAnimation.BOTTOM_TOP,*/ /*
                theme: FancyTheme.FANCY,
                gifPath: "assets/images/no_internet.gif",

                //'./assets/walp.png',
                okColor: Colors.green,
                okFun: () => {openApp()},
                ok: "Continue",
                cancelFun: () {
                  try {
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  } catch (e) {
                    print(e);
                  }
                },
             */ /* )*/ /*
            ));*/

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => AssetGiffyDialog(

            /* imagePath: 'assets/men_wearing_jacket.gif',*/
            title: Text(
              'No internet connection',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'There is no internet connection. Please turn on your internet connection and press continue.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            /* entryAnimation: EntryAnimation.RIGHT_LEFT,*/
            buttonOkText: Text("Continue"),
            onOkButtonPressed: () {
              Navigator.pop(context);
              openApp();
            },
            // buttonCancelText: ,
            onCancelButtonPressed: () {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            image: Image.asset(
              'assets/images/no_internet.gif',
              fit: BoxFit.cover,
            )));
  }
}
