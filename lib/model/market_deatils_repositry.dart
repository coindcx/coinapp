import 'dart:convert';

import 'package:coindcx/util/network/coin_network.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MarketRepositry {
  /*void loadMarketTicker(
      Function onSuccess, Function onError, Function onFailure) {
    //print("loadMarketTicker");
    CoinNetwork network = CoinNetwork.getInstnce();
    network.getMarketTicker((data) {
      saveTicker(data);
      *//* MarketDetailsModel.getInstnceData().trackerData = data;*//*
      onSuccess(data);
    }, onError, onFailure);
  }*/

  Future saveTicker(String data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("tickers", data);
  }

  Future getTicker() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString("tickers"));
  }

  Future<void> saveMarket(String data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("market", data);
  }

  Future<dynamic> getMarket() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var string = prefs.getString("market");
    return json.decode(string==null?"[]":string);
  }

  /*void loadMaretDetails(
      Function onSuccess, Function onError, Function onFailure) {
    //print("loadMarketTicker");
    CoinNetwork network = CoinNetwork.getInstnce();
    network.getMarketDetails((data) {
      //MarketDetailsModel.getInstnceData().marketData = data;
      saveMarket(data);
      onSuccess(data);
    }, onError, onFailure);
  }*/
}
