import 'dart:convert';

import 'package:coindcx/model/market_deatils_repositry.dart';
import 'package:coindcx/util/network/coin_network.dart';
import "package:collection/collection.dart";

class MarketDetailsModel {
  /* List trackerData;
  List marketData;*/
  static MarketDetailsModel instance;

  /*static getInstnceData() {
    if (instance == null) {
      instance = new MarketDetailsModel();
    }
    return instance;
  }*/

  Future<List<dynamic>> getBaseCurrancy() async {
    List xxx = await MarketRepositry().getMarket();
    ////print(marketData);
    //print(xxx.length);
    var newMap = groupBy(xxx, (obj) => obj['base_currency_short_name']);
    //print(newMap.length);
    //print("ffffffffffffffff");
    //print(newMap.keys);
    return newMap.keys.toList();
  }

  getBaseCurrancyMarket(String key) async {
    // List xxx = marketData;
    List xxx = await MarketRepositry().getMarket();
    //print(xxx.length);
    var newMap = groupBy(xxx, (obj) => obj['base_currency_short_name']);
    //print(newMap.length);
    //print("ffffffffffffffff");
    //print(newMap.keys);
    return newMap[key];
  }

  Future<List> getTrackerData() async {
    MarketRepositry marketRepositry = new MarketRepositry();
    return await marketRepositry.getTicker();
  }

  void loadData(Function marketCurrancyPairSuccess) {
    CoinNetwork network = new CoinNetwork();
    Future.delayed(Duration(seconds: 2));
    MarketRepositry marketRepositry = new MarketRepositry();
    List<Future> futures = [];
    futures.add(network.getMarketDetailsFuture());
    network = new CoinNetwork();
    futures.add(network.getMarketTickerFuture());
    Future.wait(futures /*, eagerError: true*/).then((datats) {
      //print(datats);
      var marketDataResponse = datats[0];
      var tickerDataResponse = datats[1];
      List trackerData = json.decode(tickerDataResponse.body);
      var marketData = json.decode(marketDataResponse.body);

var nullValues=[];
        /*var flag=true;*/
        for (var b in trackerData) {
          for (var a in marketData) {
          if (a["coindcx_name"].toString() == b["market"].toString()) {
            b["name"] = a["target_currency_short_name"];
            /*flag=false;*/
            break;
          }

        }
          if(b["name"]==null){
            nullValues.add(b);

          }
        /*if(flag){
          trackerData
        }*/
      }

        nullValues.forEach((element) { trackerData.remove(element);});
      marketRepositry.saveTicker(json.encode(trackerData));
      marketRepositry.saveMarket(marketDataResponse.body);
      marketCurrancyPairSuccess(trackerData);
    } /*, onError: (e) {
      //print(e);
    }*/
        );
  }

  getMarketData() async {
    MarketRepositry marketRepositry = new MarketRepositry();
    return await marketRepositry.getMarket();
  }
}
