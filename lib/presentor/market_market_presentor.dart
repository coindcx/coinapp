import 'package:coindcx/model/market_details_model.dart';
import 'package:coindcx/ui/market/table_heading_cell.dart';

class MarketDetailsPresentor {
  MarketDetailView _marketDetailView;
  MarketDetailsModel _marketDetailsModel;

  MarketDetailsPresentor(this._marketDetailView) {
    _marketDetailsModel = new MarketDetailsModel();
  }

  void getMarketCurrencyPairs() {
    _marketDetailsModel.loadData(_marketDetailView.marketCurrancyPairSuccess);
  }

  Future<void> sortKeyWise(String key, ORDER order, String baseCurrency) async {
    List x;
    if (baseCurrency != null) {
      var datax = await _marketDetailsModel
          .getTrackerData(); ////MarketDetailsModel.getInstnceData().marketData;
      var baseMarket =
          await _marketDetailsModel.getBaseCurrancyMarket(baseCurrency);
      x = [];
      for (var a in baseMarket) {
        for (var b in datax) {
          if (a["coindcx_name"].toString() == b["market"].toString()) {
            x.add(b);
            break;
          }
        }
      }
    } else {
      x = await _marketDetailsModel.getTrackerData();
    }
    x.sort((t1, t2) {
      if (order == ORDER.ASSENDING) {
        return t1[key]
            .toString()
            .toLowerCase()
            .compareTo(t2[key].toString().toLowerCase());
      } else {
        return -t1[key]
            .toString()
            .toLowerCase()
            .compareTo(t2[key].toString().toLowerCase());
      }
    });
    _marketDetailView.marketCurrancyPairSuccess(x);
  }

  Future<void> showOnlyBase(String baseCurrency) async {
    List datax = await _marketDetailsModel
        .getTrackerData();
    var baseMarket =
        await _marketDetailsModel.getBaseCurrancyMarket(baseCurrency);
    var x = [];
    for (var a in baseMarket) {
      for (var b in datax) {
        if (a["coindcx_name"].toString() == b["market"].toString()) {
          x.add(b);
          break;
        }
      }
    }
    _marketDetailView.marketCurrancyPairSuccess(x);
  }

  getBaseCurrancy() {
    return _marketDetailsModel.getBaseCurrancy();
  }

  getMarketCurrencyFor(String market) async {
    var baseMarket = await _marketDetailsModel.getMarketData();
    for (var a in baseMarket) {
      if (a["coindcx_name"].toString() == market) {
        return a;
      }
    }
    return null;
  }
}

abstract class MarketDetailView {
  void marketCurrancyPairSuccess(data);

  void marketOnError(data);

  void marketOnFailure(message);
}
