import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CurrencyPairDetailsWidget extends StatelessWidget {
  final Map marketData;

  final Map targetData;

  const CurrencyPairDetailsWidget({
    Key key,
    this.marketData,
    this.targetData,
  }) : super(key: key);

  /*
  *
  * ,*/
  @override
  Widget build(BuildContext context) {
    //var data =targetData;
    // //print(data);
    /*{
      "market": "BTCINR",
      "change_24_hour": "-1.347",
      "high": "437382.86",
      "low": "405000.01",
      "volume": "4156484.30920880087476572",
      "last_price": "422284.47",
      "bid": "419007.33000000",
      "ask": "422508.29000000",
      "timestamp": 1584546055
    }*/
    /*{
        "coindcx_name": "PAXUSDT",
        "base_currency_short_name": "USDT",
        "target_currency_short_name": "PAX",
        "target_currency_name": "Paxos",
        "base_currency_name": "Tether",
        "min_quantity": 0.01,
        "max_quantity": 900000,
        "min_price": 0.19934,
        "max_price": 4.9835,
        "min_notional": 10,
        "base_currency_precision": 4,
        "target_currency_precision": 2,
        "step": 0.01,
        "order_types": [
          "take_profit",
          "stop_limit",
          "market_order",
          "limit_order"
        ],
        "symbol": "PAXUSDT",
        "ecode": "B",
        "max_leverage": 3,
        "max_leverage_short": null,
        "pair": "B-PAX_USDT",
        "status": "active"
      }*/

    final Widget networkSvg = SvgPicture.network(
      'https://coindcx.com/assets/coins/${targetData["name"]}.svg',
      semanticsLabel: 'A shark?!',
      height: 80,
      width: 80,
      placeholderBuilder: (BuildContext context) => Container(
          padding: const EdgeInsets.all(10.0),
          child: const CircularProgressIndicator()),
    );
    return /*Scaffold(
      body:*/
        Container(
      child: Container(
        decoration: new BoxDecoration(
           /* color: Colors.white,*/
            borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(10.0),
                topRight: const Radius.circular(10.0))),
        margin: EdgeInsets.fromLTRB(20, 30, 20, 30),
        child: new Wrap(
          children: <Widget>[
            Column(
              children: <Widget>[
                Center(
                  child:
                      /*new Icon(
                    Icons.music_note,
                    size: 50,
                  )*/
                      networkSvg,
                ),
                SizedBox(
                  height: 10,
                ),
                new Text(
                  marketData['target_currency_name'],
                  style: TextStyle(color: Colors.black87, fontSize: 24),
                ),
                SizedBox(
                  height: 5,
                ),
                new Text(
                  marketData['coindcx_name'],
                  style: TextStyle(color: Colors.black54, fontSize: 20),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      /*color: Colors.green,*/
                      border: Border.all(
                        color: Colors.green,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Buy",
                          style: TextStyle(
                              color: Colors.green,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(targetData["bid"],style: TextStyle(fontSize: 16),)
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  decoration: BoxDecoration(
                      /*color: Colors.green,*/
                      border: Border.all(
                        color: Colors.red,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                    child: Column(
                      children: <Widget>[
                        Text("Sell",
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                        SizedBox(
                          height: 5,
                        ),
                        Text(targetData["ask"],style: TextStyle(fontSize: 16),)
                      ],
                    ),
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceAround,
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    /* Container(
                 */ /*  margin: EdgeInsets.all(5),*/ /*
                   child: */
                    Row(
                      children: <Widget>[
                        Text(
                          "Last Price: ",
                          style: TextStyle(color: Colors.black87, fontSize: 16),
                        ),
                        SizedBox(
                          /*  height: 5*/
                          width: 5,
                        ),
                        Text(
                          targetData["last_price"],
                          style: TextStyle(color: Colors.black87, fontSize: 16),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    /* ),*/
                    Row(
                      children: <Widget>[
                        Text(
                          "24h Changes: ",
                          style: TextStyle(color: Colors.black87, fontSize: 16),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          targetData["change_24_hour"],
                          style: TextStyle(color: Colors.black87, fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Low: ",
                          style: TextStyle(color: Colors.red, fontSize: 16),
                        ),
                        SizedBox(
                          height: 5,
                          width: 5,
                        ),
                        Text(
                          targetData["low"],
                          style: TextStyle(color: Colors.red, fontSize: 16),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),

                    Row(
                      children: <Widget>[
                        Text(
                          "High: ",
                          style: TextStyle(color: Colors.green, fontSize: 16),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          targetData["high"],
                          style: TextStyle(color: Colors.green, fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
              ],
            )

            /*  Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Last Price",
                        style: TextStyle(color: Colors.black87, fontSize: 18),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "2000",
                        style: TextStyle(color: Colors.black87, fontSize: 18),
                      ),
                    ],
                  ),
                ),
                Column(
                  children: <Widget>[
                    Text("24 Hr High"),
                    Text("2000"),
                  ],
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            )*/
          ],
        ),
        /*  ),*/
      ),
    /*  color: Colors.transparent,*/
    );
  }
}
