import 'package:coindcx/presentor/market_market_presentor.dart';
import 'package:coindcx/ui/market/table_cell_data.dart';
import 'package:coindcx/ui/market/table_heading_cell.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:toast/toast.dart';

import 'currency_pair_details_widget.dart';

class MarketDetailPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MarketDetailState();
  }
}

class MarketDetailState extends State<MarketDetailPage>
    implements MarketDetailView {
  var trackersList = []; //=App.getData();
  ScrollController _scrollController = new ScrollController(
    initialScrollOffset: 0.0,
    keepScrollOffset: true,
  );
  int count = 100;
  String baseCurrency;
  MarketDetailsPresentor _marketDetailsPresentor;
  List baseCurrencyList = [];

  @override
  void initState() {
    _marketDetailsPresentor = new MarketDetailsPresentor(this);
    _marketDetailsPresentor.getMarketCurrencyPairs();
    /* var a =_marketDetailsPresentor.getBaseCurrancy();*/
    super.initState();
    _scrollController.addListener(() {
      //print("xxxx");
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget networkSvg = SvgPicture.asset(
      'assets/images/app_icon.svg',
      semanticsLabel: 'A shark?!',
      height: 48,
      width: 48,
      placeholderBuilder: (BuildContext context) => Container(
          padding: const EdgeInsets.all(10.0),
          child: const CircularProgressIndicator()),
    );
    return Scaffold(
      appBar: AppBar(
        leading: Container(
          padding: EdgeInsets.all(10),
          child: networkSvg,
        ),
        actions: <Widget>[
          MaterialButton(
            onPressed: () async {
              //_marketDetailsPresentor.getMarketCurrencyPairs();
              baseCurrency = await _asyncSimpleDialog(context);
              //print(baseCurrency);
              // //print(MarketDetailsModel.getInstnceData().getBaseCurrancyMarket(baseCurrency));
              //  trackersList = MarketDetailsModel.getInstnceData().trackerData;
              updateWith(baseCurrency);
            },
            child: Row(
              children: <Widget>[
                /* Icon(*/
                Image.asset(
                  "assets/images/conversion_icon.png",
                  width: 30,
                  height: 30,
                ),
                SizedBox(
                  width: 5,
                ),
                /*),*/
                Text(
                  baseCurrency == null ? "ALL" : baseCurrency,
                  style: TextStyle(fontSize: 18, color: Colors.white),
                )
              ],
            ),
          )
        ],
        title: Text("CoinDCX"),
      ),
      body: Column(
        children: <Widget>[
          Table(
            children: [headingItemGenrator()],
          ),
          Expanded(
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Table(children: getAllRows()),
            ),
          )
        ],
      ),
      /*)*/
    );
  }

  TableRow headingItemGenrator() {
    return TableRow(children: [
      TableHeadingCell(
          title: "Pair",
          onPress: (order) {
            //print(order);
            sortKeyWise(
              "market",
              order,
            );
          }),
      TableHeadingCell(
          title: "Price",
          onPress: (order) {
            //print(order);
            sortKeyWise(
              "last_price",
              order,
            );
          }),
      TableHeadingCell(
          title: "24h Chg",
          onPress: (order) {
            //print(order);
            sortKeyWise(
              "change_24_hour",
              order,
            );
          }),
    ]);
  }

  TableRow genrateColumn(data) {
    var market = data["market"].toString();
    var name = data["name"].toString();
    var price = data["last_price"].toString();
    var p24Price = data["change_24_hour"].toString();
    return TableRow(children: [
      TableDataIconCell(
          title: market,
          name: name,
          onPress: () {
            openBottomSheet(data);
          }),
      TableDataCell(
          title: price,
          onPress: () {
            openBottomSheet(data);
          }),
      TableDataCellColored(
          title: p24Price,
          onPress: () {
            openBottomSheet(data);
          }),
    ]);
  }

  List<TableRow> getAllRows() {
    List<TableRow> widgetRows = [];
    int x = 0;
    for (var data in trackersList) {
      widgetRows.add(genrateColumn(data));
      x++;
      /* if (x == count) {
        break;
      }*/
    }
    //print(trackersList.length);
    return widgetRows;
  }

  void sortKeyWise(String key, ORDER order) {
    _marketDetailsPresentor.sortKeyWise(key, order, baseCurrency);
  }

  Future<dynamic> _asyncSimpleDialog(BuildContext context) async {
    var baseCurrencyList = await _marketDetailsPresentor.getBaseCurrancy();
    //print(baseCurrencyList);
    return await showDialog<dynamic>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Select Base Currency '),
            children: getOptions(baseCurrencyList),
          );
        });
  }

  List<Widget> getOptions(List baseCurrencyList) {
    List<Widget> optionList = [];

    for (var item in baseCurrencyList) {
      optionList.add(SimpleDialogOption(
        onPressed: () {
          //print(item);
          Navigator.pop(context, item.toString());
        },
        child: Text(item.toString(),style: TextStyle(fontSize: 16),),
      ));
    }
    return optionList;
  }

  void updateWith(String baseCurrency) {
    _marketDetailsPresentor.showOnlyBase(baseCurrency);
  }

  @override
  marketCurrancyPairSuccess(data) {
    if (data == null) {
      //print(data);
    } else {
      trackersList = data;
      setState(() {});
    }
  }

  void openBottomSheet(data) {
    //print(data);
    _settingModalBottomSheet(context, data);
  }

  Future<void> _settingModalBottomSheet(context, data) async {
    var targetData = data;
    //print(targetData);
    var marketData =
        await _marketDetailsPresentor.getMarketCurrencyFor(data["market"]);
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        context: context,
        builder: (BuildContext bc) {
          return CurrencyPairDetailsWidget(
              marketData: marketData, targetData: targetData);
        });
  }

  @override
  void marketOnError(data) {
    Toast.show("Internal server error", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  @override
  void marketOnFailure(data) {
    //print("sssssssssssssssss");
    //print(data);
    Toast.show("Please check the internet connection", context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }
}
