import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TableDataCell extends StatelessWidget {
  final Function onPress;

  const TableDataCell({
    Key key,
    @required this.title,
    this.onPress,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
        /*Center(
      heightFactor: 1.2,
      child: Container(
        alignment: Alignment(0.0, 0.0),
      */ /*height: double.infinity,*/ /*
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[*/
        child: GestureDetector(
          onTap: () {
            onPress();
          },
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Center(
              child: Text(
                title,
                style: TextStyle(color: Colors.black, fontSize: 18),
              ),
            ),
          ]),
        )
        /*],
      ),
      color: Colors.black87,
    ),)*/
        );
  }
}

class TableDataCellColored extends StatelessWidget {
  final Function onPress;

  const TableDataCellColored({
    Key key,
    @required this.title,
    this.onPress,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    var number = double.tryParse(title) == null ? 0 : double.tryParse(title);
    //print(title);
    //print(number);
    //print(double.tryParse(title.trim()));
    return Container(
      child: GestureDetector(
        onTap: () {
          onPress();
        },
        child: Container(
          /*  width: double.infinity,*/
          child: Center(
            child: Text(
              number.toString(),
              style: TextStyle(
                  color: number >= 0 ? Colors.green : Colors.red, fontSize: 18),
            ),
          ),
        ),
      ),
      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
    );
  }
}

class TableDataIconCell extends StatelessWidget {
  final Function() onPress;

  const TableDataIconCell({
    Key key,
    @required this.title,
    this.onPress,
    this.name,
  }) : super(key: key);

  final String title;
  final String name;

/*https://coindcx.com/assets/coins/inr.svg*/
  @override
  Widget build(BuildContext context) {
    final Widget networkSvg = SvgPicture.network(
      'https://coindcx.com/assets/coins/${name}.svg',
      semanticsLabel: 'A shark?!',
      height: 30,
      width: 30,
      placeholderBuilder: (BuildContext context) => Container(
          /*  padding: const EdgeInsets.all(5.0),*/
          child: const CircularProgressIndicator()),
    );

    return GestureDetector(
      onTap: () {
        onPress();
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(4, 10, 0, 10),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              networkSvg,
              SizedBox(
                width: 5,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    name,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    title,
                    style: TextStyle(color: Colors.black54, fontSize: 14),
                  ),
                ],
              )
            ]),
      ),
    );
  }
}
