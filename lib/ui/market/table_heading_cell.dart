import 'package:flutter/material.dart';

class TableHeadingCell extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TableHeadingCellState(title: title,onPress: onPress);
  }

  final Function(ORDER) onPress;

  final String title;

  const TableHeadingCell({
    Key key,
    @required  this.onPress,
    @required this.title,
  }) : super(key: key);
}

enum ORDER { ASSENDING, DESCENDING }

class TableHeadingCellState extends State<TableHeadingCell> {
  final double iconSize = 30.0;

  final Function(ORDER) onPress;

  final String title;
  ORDER order = ORDER.ASSENDING;
  TableHeadingCellState({
    Key key,
    this.onPress,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: Row(
      children: [
        Text(
          title, style: TextStyle(color: Colors.black,fontSize: 18),
        ),
        IconButton(
          icon: Icon(

            order == ORDER.ASSENDING
                ? Icons.arrow_upward
                : Icons.arrow_downward,
            size: iconSize,
          ),
          onPressed: () {
            order =  order == ORDER.ASSENDING?ORDER.DESCENDING: ORDER.ASSENDING;
            setState(() {});
            onPress(order);
          },
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    ),);
  }
}