import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';

abstract class BaseNetwork {
  Future<Response> getRestApi(String url, {Map param}) async {
    //print(url);
    return http.get(url);
  }

  void getRestApiWithCallBack(
      String url, Function onSuccess, Function onError, Function onFailure) {
    //print("getRestApiWithCallBack");
    //print(url);
    getRestApi(url).then((value) {
      //print("dddddddddddddd");
      //print(value.statusCode);
      try {
        if (value.statusCode == 200 ||
            value.statusCode == 201 ||
            value.statusCode == 202 ||
            value.statusCode == 203) {
          onSuccess(json.decode(value.body));
        } else {
          onError(json.decode(value.body));
        }
      } catch (e) {
        //print(e);
        onFailure("Internal server error");
      }
    }).catchError((e) {
      //print(e);
      //print(url);
      //print("dddddddddddddddddd");
      onFailure("Internal server error");
    });
  }
}
