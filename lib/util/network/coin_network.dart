import 'package:http/http.dart';

import 'base_network.dart';

class CoinNetwork extends BaseNetwork {
  /*Market details API : /exchange/v1/markets_details
    Ticker API : /exchange/ticker*/
  String TRICKER_API = "exchange/ticker";
  String Market_API = "exchange/v1/markets_details";
  String baseUrl = "https://api.coindcx.com/";

  static var _instance;

  static CoinNetwork getInstnce() {
    if (_instance == null) {
      _instance = new CoinNetwork();
    }
    return _instance;
  }

  void getMarketDetails(
      Function onSuccess, Function onError, Function onFailure) {
    getRestApiWithCallBack(baseUrl + Market_API, onSuccess, onError, onFailure);
  }

  void getMarketTicker(
      Function onSuccess, Function onError, Function onFailure) {
    //print("getMarketTicker");
    getRestApiWithCallBack(
        baseUrl + TRICKER_API, onSuccess, onError, onFailure);
  }

  Future<Response> getMarketTickerFuture() {
    //print("getMarketTickerFuture");
    return getRestApi(baseUrl + TRICKER_API);
  }

  Future<Response> getMarketDetailsFuture() {
    //print("getMarketDetailsFuture");
    return getRestApi(baseUrl + Market_API);
  }
}
